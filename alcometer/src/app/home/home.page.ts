import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  genders = []
  hours = []
  bottles = []

  weight: number
  gender: string
  time: number
  bottle: number
  promilles: number

  constructor() {}

  ngOnInit() {
    this.genders.push("Male")
    this.genders.push("Female")
    this.gender = "Male"

    for (let i = 0; i <= 24; i++) {
      this.hours.push(i)
    }

    for (let i = 0; i <= 24; i++) {
      this.bottles.push(i)
    }    
  }

  calculate() {
    const litres = this.bottle * 0.33
    let grams = litres * 8 * 4.5
    const burning = this.weight / 10 
    grams = grams - (burning * this.time) 

    if (this.gender === "Male") {
      this.promilles = grams / (this.weight * 0.7) 
    } else {
      this.promilles = grams / (this.weight * 0.6) 
    }
  }

}