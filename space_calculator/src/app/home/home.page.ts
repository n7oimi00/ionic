import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  totalSpace: number
  usedSpace: number
  tracks: number
  remainingSpace: number
  averageTrack: number
  remainingTracks: number

  constructor() {}

  calculate () {
    this.remainingSpace = this.totalSpace - this.usedSpace
    this.averageTrack = this.usedSpace / this.tracks
    this.remainingTracks = Math.floor(this.remainingSpace / this.averageTrack)
  }

}
