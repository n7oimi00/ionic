import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DbService } from '../db.service';
import { Question } from '../../question';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {
  allQuestions: Question[]
  currentQuestion: Question
  questionCounter: number
  score: number
  answers: string[]
  time: number = 10
  interval: any
  
  constructor(private db: DbService, public router: Router) { }

  ngOnInit() {
    this.initialize()
  }

  initialize() {
    this.db.getAll()
    .then(json => {
      this.allQuestions = json})
      .then(() => {
        this.suffle(this.allQuestions)
        this.allQuestions.splice(5)
        this.allQuestions.map((question) => {this.suffle(question.options)})
        this.questionCounter = 0
        this.currentQuestion = this.allQuestions[this.questionCounter]
        this.score = 0
        this.answers = []             
        this.questionCounter++ 
        this.interval = setInterval(() => {
          if (this.time > 0) {
            this.time--
          } else {
            this.answers.push("Timed out")
            this.setQuestion()
          }          
        }, 1000)
      })   
    }

  setQuestion() {
    if (this.questionCounter === this.allQuestions.length) {
      clearInterval(this.interval)
      this.db.setAnswers(this.answers)
      this.db.setQuestions(this.allQuestions)
      this.router.navigateByUrl('result/' + this.score)
    } else {
    this.currentQuestion = this.allQuestions[this.questionCounter]
    this.questionCounter++
    this.time = 10
    }
  }  

  handleAnswer(answer: string, question: Question) {
    if (answer === question.correctOption) {
      this.score++
    }
    this.answers.push(answer)
    this.setQuestion()
  }  

  suffle(array: any) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];      
    }
  }    
}  