import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';
import { Question } from '../../question';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.page.html',
  styleUrls: ['./answers.page.scss'],
})
export class AnswersPage implements OnInit {
  questions: Question[]
  answers: string[]

  constructor(private db: DbService) { }

  ngOnInit() {
    this.questions = this.db.getQuestions()
    this.answers = this.db.getAnswers()
  }

}
