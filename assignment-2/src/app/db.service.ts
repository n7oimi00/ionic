import { Injectable } from '@angular/core';
import { Question } from '../question';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  answers: string[]
  questions: Question[]

  constructor() { }

  getAll() {
    return fetch('../../assets/data/questions.json').then((response => response.json()))
  }

  setAnswers(answers: string[]) {
    this.answers = answers
  }

  getAnswers() {
    return this.answers
  }

  setQuestions(questions: Question[]) {
    this.questions = questions
  }

  getQuestions() {
    return this.questions
  }
}
