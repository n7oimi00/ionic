import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  constructor(public router: Router, public activatedRoute: ActivatedRoute) { }
  score: number
  rank: string
  imgPath: string

  ngOnInit() {
    this.score = Number(this.activatedRoute.snapshot.paramMap.get('score'))
    if (this.score === 5) {
      this.rank = "Prince"
      this.imgPath = "../assets/images/prince.png"
    } else if (this.score === 4) {
      this.rank = "Duke"
      this.imgPath = "../assets/images/duke.png"
    } else if (this.score === 3) {
      this.rank = "Earl"
      this.imgPath = "../assets/images/earl.png" 
    } else if (this.score === 2) {
      this.rank = "Viscount"
      this.imgPath = "../assets/images/viscount.png" 
    } else if (this.score === 1) {
      this.rank = "Baron"
      this.imgPath = "../assets/images/baron.png"
    } else {
      this.rank = "Peon"
    }
  }

}
