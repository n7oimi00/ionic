export class Question {
    question: string
    options: string[]
    correctOption: string
    explanation: string
}