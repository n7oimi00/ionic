import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MovieQuestion } from '../../MovieQuestion';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  questions: MovieQuestion[] = []
  activeQuestion: MovieQuestion
  isCorrect: boolean
  feedback: string
  questionCounter: number
  optionCounter: number

  startTime: Date
  endTime: Date
  duration: number

  constructor(public router: Router) {}

  ngOnInit() {
    this.questions = [{
      quote: "I'm still standing",
      options: [
        "The Godfather",
        "Scarface",
        "Scent of a Woman",
        "Glengarry Glen Ross" 
      ],
      correctOption: 1  
    },
    {
      quote: "Frankly, my dear, I don't give a damn",
      options: [
        "King Kong",
        "The Maltese Falcon",
        "Gone with the Wind",
        "Casablanca"
      ],
      correctOption: 2
    },
    {
      quote: "I'm simply saying that life, uh, finds a way",
      options: [
        "Jurassic Park",
        "Forrest Gump",
        "Independence Day"
      ],
      correctOption: 0
    }
    ]

    this.questionCounter = 0
    this.setQuestion()
    this.startTime = new Date()
  } 

  setQuestion() {
    if (this.questionCounter === this.questions.length) {
      this.endTime = new Date()
      this.duration = this.endTime.getTime() - this.startTime.getTime()
      this.router.navigateByUrl('result/' + this.duration)
      this.ngOnInit()

    } else {
    this.optionCounter = 0
    this.feedback = ""
    this.isCorrect = false
    this.activeQuestion = this.questions[this.questionCounter]
    this.questionCounter++
    }
  }

  checkOption(option: number, activeQuestion: MovieQuestion) {
    this.optionCounter++
    if (this.optionCounter > activeQuestion.options.length) {
      this.setQuestion()
    }
    if (option === activeQuestion.correctOption) {
      this.isCorrect = true
      this.feedback = activeQuestion.options[option] + " is correct. Tap the projector."
    } else {
      this.isCorrect = false
      this.feedback = "Incorrect. Quesses left: " + (this.activeQuestion.options.length - this.optionCounter) 
    }
  }

}